var express = require("express");
var router = express.Router();
var Models = require("../Models.js");

router.get("/",function(req,res)
{
    res.send("Sorry, this was an inappropriate url...<br>Please click <a href='/'>here</a> to return to the index...");
});

router.get("/otp/:otp",function(req,res)
{
    var otp = Number(req.params.otp);
    res.render("actor.html");
});

// end
module.exports = router;