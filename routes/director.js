var express = require("express");
var router = express.Router();
var Models = require("../Models");

router.get("/",function(req,res) // Landing page
{
    res.send("Sorry, this was an inappropriate url...<br>Please click <a href='/'>here</a> to return to the index...");
});

router.get("/otp/:otp",function(req,res)
{
    var otp = Number(req.params.otp);
    Models.DirectorData.findOne({otp:otp},function(err,director)
    {
        if (err)
        {
            console.log(err);
            res.send("Something went wrong, please check the server console...");
            return;
        }

        if (!director) // If there was no director matching this otp
        {
            res.redirect("/");
        }
        else
        {
            res.render("director.html");
        }
    });
});

router.get("/scene/:scene_id",function(req,res)
{
    var scene_id = req.params.scene_id;

    res.render("director_scene.html");
});

router.get("/setup/:scene",function(req,res) // display setup questions for a specific scene
{
    
});


/* END */
module.exports = router;