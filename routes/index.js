var express = require('express');
var router = express.Router();
var request = require("request");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.html');
});

/*router.get("/qr-login/:otp",function(req,res) // replaced by /api/testOTP/:otp
{
  var otp = req.params.otp;
  
});*/

module.exports = router;
