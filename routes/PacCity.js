var express = require("express");
var router = express.Router();
var Models = require("../Models.js");

router.get("/",function(req,res)
{
    res.render("PacCity.html")
});

router.get("/admin/8879175189",function(req,res)
{
    res.render("PacAdmin.html");
});

router.get("/audience",function(req,res)
{
    res.render("audience.html");
})
// APIs below

router.post("/comment",function(req,res)
{
    var data = req.body;
    console.log(data);
    var message = new Models.Messages(data);
    message.save(function(err)
    {
        if (err)
        {
            console.log(err);
            res.send("There was an error, please ask admin to check the server console");
        }
        else
        {
            res.send("Thank you for your comment!");
        }
    })
});

router.get("/performers",function(req,res)
{
    Models.Performer.find({},function(err,performers)
    {
        if (err)
        {
            console.log(err);
            res.send(null);
        }
        else
        {
            var to_send = [];
            performers.map(function(performer)
            {
                to_send.push(performer.name);
            });
            res.send(to_send);
        }
    });
});

router.get("/login/:number",function(req,res)
{
    var number = Number(req.params.number);
    Models.Audience.findOne({phone_number:number},function(err,audience)
    {
        if (err)
        {
            console.log(err);
            res.send(null);
        }
        else
        {
            if (audience) // if present
            {
                res.send(audience)
            }
            else
            {
                res.send(null);    
            }
        }
    });
});

router.post("/addPerformer",function(req,res)
{
    var data = req.body;
    var to_send = {}
    var audience = new Models.Audience(data); // make audience first
    audience.save(function(err)
    {
        if (err)
        {
            console.log(err)
            res.send(null);
        }
        else
        {
            data.mids = [];
            var performer = new Models.Performer(data); // make performer
            performer.save(function(err)
            {
                if (err)
                {
                    console.log(err);
                    res.send({audience:audience,performer:null});
                }
                else
                {
                    res.send({audience:audience,performer:performer});
                }
            });
        }
    });

    
});

router.post("/addAudience",function(req,res)
{
    var data = req.body;
    var audience = new Models.Audience(data);
    audience.save(function(err)
    {
        if (err)
        {
            console.log(err);
            res.send(null);
        }
        else
        {
            res.send(audience);
        }
    })
});

module.exports = router;