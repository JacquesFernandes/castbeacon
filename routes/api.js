var express = require("express");
var router = express.Router();
var Models = require("../Models.js");

router.get("/",function(req,res)
{
    res.send("Sorry, this is a non-UI api");
});

router.get("/director/:otp",function(req,res)
{
    console.log("FETCHING DIRECTOR WITH: "+req.params.otp);
    var otp = Number(req.params.otp);

    Models.DirectorData.findOne({otp:otp},function(err,director)
    {
        if (err)
        {
            console.log(err);
            res.send("error");
        }

        if (director) // a matching director was found
        {
            res.send(director);
        }
        else
        {
            res.send(null);
        }
    });
});

router.get("/actor/:otp",function(req,res)
{
    console.log("FETCHING ACTOR WITH: "+req.params.otp);
    var otp = Number(req.params.otp);

    Models.ActorData.findOne({otp:otp},function(err,actor)
    {
        if (err)
        {
            console.log(err);
            res.send("error");
        }

        if (actor) // a matching director was found
        {
            res.send(actor);
        }
        else
        {
            res.send(null);
        }
    });
});


router.get("/scene/:scene_id",function(req,res)
{
    Models.Scene.findOne({scene_id:req.params.scene_id},function(err,scene)
    {
        if (err)
        {
            console.log(err);
            res.send(null);
        }
        else
        {
            if (scene) // scene was found
            {
                res.send(scene);
            }
            else
            {
                res.send(null);
            }
        }
    });
});

router.get("/testOTP/:otp",function(req,res) // checks if valid and what type
{
    var otp = Number(req.params.otp);
    Models.OtpType.findOne({otp:otp},function(err,result)
    {
        if (err)
        {
            if (err.name === "CastError")
            res.send({error:"Bad Value"});
            return;
        }

        if (result.type_of_person)
        {
            let top = result.type_of_person; // local use top -> type_of_person
            if (top === "director")
            {
                //req.cookies.person = "director";
                res.send("/director/otp/"+otp);
            }
            else if (top === "actor")
            {
                //req.cookies.person = "actor";
                res.send("/actor/otp/"+otp);
            }
        }
    });
    //res.send("you sent:"+req.params.otp);
});

router.post("/updateBrief/:scene_id",function(req,res)
{
    var new_brief = req.body.text;
    console.log(new_brief)
    var scene_id = req.params.scene_id;
    Models.Scene.findOne({scene_id:scene_id},function(err,scene)
    {
        if (err)
        {
            console.log(err);
            res.send("ERROR: Check server console....");
            return;
        }

        if (scene) // scene was found
        {
            scene.brief = new_brief;
            scene.save(function(err)
            {
                if (err)
                {
                    console.log(err);
                }
                else
                {
                    console.log(scene);
                    res.send(scene);
                }
            });
        }
        else
        {
            res.send("Error: No scene with id "+scene_id+" was found...");
            return;
        }
    });
});

router.post("/addActorData",function(req,res)
{
    /*var data = { // what is being sent
        actor_id:id,
        otp:otp,
        name:name,
        email:email,
        phone:phone,
        sex:sex,
        dob:dob,
        scene_id:scene_id
    };*/

    var data = req.body;
    
    // cleaning
    data.otp = Number(data.otp);

    var ActorData = Models.ActorData;
    var new_actor = new ActorData(data);
    new_actor.save(function(err)
    {
        if (err)
        {
            console.log(err);
        }
    });

    var OtpType = Models.OtpType;
    var new_otp = new OtpType({otp:data.otp, type_of_person:"actor", name:data.name});
    new_otp.save(function(err)
    {
        if (err)
        {
            console.log(err);
        }
    });

    res.send("Added Actor: "+JSON.stringify(new_actor));
});

router.post("/addScene",function(req,res)
{
    /*
    scene_data.scene_id = document.getElementById("scene_id").value;
    scene_data.director = document.getElementById("director").value;
    scene_data.brief = document.getElementById("brief").value;
    scene_data.actors = scene_data.actors.split(", ");
    */
    var data = req.body;
    data.actors = data["actors[]"]; // why the hell it does this, I have no idea
    delete(data["actors[]"]);
    
    var Scene = Models.Scene;
    var new_scene = new Scene(data);
    
    new_scene.save(function(err)
    {
        if (err)
        {
            console.log(err);
        }
        else
        {
            //console.log(new_scene);
        }
    });

    Models.DirectorData.findOne({name:data.director},function(err,result)
    {
        if (err)
        {
            console.log(err);
            return;
        }

        if (result)
        {
            console.log("updating: "+JSON.stringify(result));
            result.scenes.push(data.scene_id);
            console.log("Saving: "+JSON.stringify(result));
            result.save(function(err)
            {
                if (err)
                {
                    console.log(err);
                    res.send("An Error occurred. Check logs")
                    return;
                }
                else
                {
                    res.send("Added Scene: "+JSON.stringify(new_scene));
                }
            });
        }

    });

    //res.send("Added Scene: "+JSON.stringify(new_scene));
});

router.post("/addDirector",function(req,res)
{
    /*
    director_data.otp = document.getElementById("dir_otp").value;
    director_data.name = scene_data.director;
    director_data.type_of_person = "director";
    */

    var data = req.body;
    
    data.otp = Number(data.otp);

    var OtpType = Models.OtpType;
    OtpType.findOne({otp:data.otp},function(err,result)
    {
        if (err)
        {
            console.log(err);
            return;
        }

        if (!result) // if result is null (i.e. no document found) then create new one
        {
            var new_otp = new OtpType(data)
            new_otp.save(function(err)
            {
                if (err)
                {
                    console.log(err);
                }
            });
        }
    })

    var DirectorData = Models.DirectorData;
    DirectorData.findOne({name:data.name, otp:data.otp},function(err, result)
    {
        if (err)
        {
            console.log(err);
            return;
        }

        if (!result) // No director was found matching this name and otp
        {
            var new_director = new DirectorData({otp:data.otp, name:data.name, scenes:[]}); 
            new_director.save(function(err)
            {
                if (err)
                {
                    console.log(err);
                }

                res.send("Added Director with data: "+JSON.stringify(new_director));
                return;
            });
        }
        else // Director exists
        {
            res.send("Director: "+data.name+" exists");
        }
    });
});

module.exports = router;