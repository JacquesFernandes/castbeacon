var mongoose = require("mongoose");
var Schema = mongoose.Schema;
//var model = mongoose.model;

var Models = {
    OtpType: mongoose.model("OtpType",new Schema({
        otp: {type:Number, required:true},
        type_of_person: {type:String}, // "actor" or "director",
    })),
    Scene: mongoose.model("Scene",new Schema({
        scene_id: {type:String}, // Generated
        director: {type:String},
        brief: {type:String, default:"Nothing put up yet"},
        actors: {type:[String]} // Names
    })),
    ActorData: mongoose.model("ActorData",new Schema({
        actor_id: {type:String, required:true}, // Generated
        otp: {type:Number, required:true}, // Generated
        name: {type:String},
        email: {type:String},
        phone: {type:String},
        sex: {type:String},
        dob: {type:String},
        scene_id: {type:String, required:true}
    })),
    DirectorData: mongoose.model("DirectorData",new Schema({
        otp: {type:Number, required:true},
        name: {type:String},
        scenes: {type:[String]} // List of <scene_id>s
    })),
    /*
        Audience will log in, be greeted and select a performer to send feedback to
    */
    Messages: mongoose.model("Messages",new Schema({
        to_artist: {type:String, required:true},
        message: {type:String},
        from_name: {type:String, required:true} // phone number of who sent the message
    })),
    Audience: mongoose.model("Attendees",new Schema({ // can contain audience as well as performer values
        phone_number: {type:Number, required:true},
        name: {type:String, required:true}
    })),
    Performer: mongoose.model("Performers",new Schema({
        name: {type:String, required:true},
        mids: {type:[String]}, // List of Message Ids for this performer
        phone_number: {type:Number, required:true}
    }))
};

module.exports = Models;