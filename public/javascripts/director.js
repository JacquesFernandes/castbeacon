/*
This code is for setup purposes. It will fetch data and store it in the sessionStorage for the react code to use
*/

function fetchData()
{
    var director = null; // director object
    var otp = window.location.href.split("/").pop();
    console.log(otp);
    // fetch director's name and scene_id list 
    $.ajax({
        url:"/api/director/"+otp,
        method:"GET",
        success:function(response)
        {
            director = response;
            name = director.name;
            window.sessionStorage.setItem("name",name); // Director Name
            console.log("Loaded Director Name");
            window.sessionStorage.setItem("scenes",director.scenes) // Scene_ids
            console.log("Loaded Scene ids"+JSON.stringify(director.scenes));
            director.scenes.map(function(scene_id)
            {
                $.ajax({
                    url:"/api/scene/"+scene_id,
                    method:"GET",
                    success: function(scene)
                    {
                        if (scene !== null)
                        {
                            window.sessionStorage.setItem(scene_id,JSON.stringify(scene));
                            console.log("loaded "+JSON.stringify(scene));
                        }
                        else
                        {
                            alert("Error, got a null value. Check Server console (or no scene was found)");
                        }
                    }
                });
            });
        }
    })
}

// Auto-run code below
if (!window.sessionStorage.getItem("type_of_person")) // if this is not set
{
    window.sessionStorage.setItem("type_of_person","director");
}

if (window.sessionStorage.getItem("type_of_person") !== "director")
{
    alert("Nice try, please sign in and come back :)")
    window.location = "/";
}

var otp = window.location.href.split("/").pop();
var name = null;
window.sessionStorage.setItem("otp", otp); // set's the otp in local storage
fetchData();