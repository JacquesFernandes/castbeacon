class Greeting extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <div>
                <font style={{fontSize:"3em"}}>Welcome {this.props.name},</font>
            </div>
        );
    }
}
var check_greeting = setInterval(function()
{
    if (window.sessionStorage.getItem("name"))
    {
        var greeting = React.createElement(Greeting,{name:window.sessionStorage.getItem("name")});
        ReactDOM.render(greeting,document.getElementById("greeting"));
        clearInterval(check_greeting);
    }
},250);

class FeedbackRoot extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        var performers = window.sessionStorage.getItem("performers"); // string
        return(
            <div>
                <Selector performers={performers}/><br />
                <CommentBox />
            </div>
        );
    }
}

class Selector extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        var performers = JSON.parse(this.props.performers);
        console.log(performers);
        return(
            <div>
            <font style={{fontSize:"1.9em"}}>Performers:</font><br/>
                {
                    performers.map(function(performer)
                    {
                        return(
                            <div>
                                <input type="radio" name="performer" style={{marginLeft:"2em"}} value={performer}/><font style={{fontSize:"1.9em"}}>{performer}</font>
                            </div>
                        )
                    })
                }
            </div>
        );
    }

}

class CommentBox extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <div>
            <br/>
                <font style={{fontSize:"1.9em"}}>Comment:</font><br/>
                <textarea id="comment" style={{width:"100%", fontSize:"1.9em"}} placeholder="Your thoughts and suggestions here"></textarea>
            </div>
        )
    }
}
var feedback_checker = setInterval(function()
{
    if (window.sessionStorage.getItem("performers"))
    {
        var feedback_root = React.createElement(FeedbackRoot);
        ReactDOM.render(feedback_root, document.getElementById("feedback"));
        clearInterval(feedback_checker);
    }
},250);