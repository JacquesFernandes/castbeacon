class Greeting extends React.Component
{
    constructor()
    {
        super();
        //this.name = window.sessionStorage.getItem("name");
    }

    render()
    {
        return(
        <div>
            <font style={{fontSize:"2.5em"}}>Welcome {this.props.name}! <br/>Here's your brief for today...</font>
        </div>
        );
    }
}
var greeting_checker = setInterval(function()
{
    if (window.sessionStorage.getItem("name")) // name is set
    {
        var greeting = React.createElement(Greeting,{name:window.sessionStorage.getItem("name")});
        console.log("Rendering greeting");
        ReactDOM.render(greeting,document.getElementById("greeting"));
        clearInterval(greeting_checker);
    }
},250);

class Brief extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <div style={{margin:7}}>
                <p>
                    <font style={{fontSize:"2em", fontStyle:"italic"}}>
                        {"\""+this.props.brief_text+"\""}
                    </font>
                </p>
            </div>
        );
    }
}
var brief_checker = setInterval(function()
{
    if (window.sessionStorage.getItem("brief"))
    {
        var brief = React.createElement(Brief,{brief_text:window.sessionStorage.getItem("brief")});
        ReactDOM.render(brief,document.getElementById("scene"));
        clearInterval(brief_checker);
    }
},250);