class Head extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <font style={{fontSize:"6em"}}>CastBeacon</font>
        );
    }
}

class Body extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <Tiles />
        );
    }
}

class Tiles extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <table width="100%" height="100%">
                <tr >
                {this.renderTile("Director","/director","#d5f4e6")}
                {this.renderTile("Actor","/actor","#80ced6")}
                </tr>
            </table>
        );
    }

    renderTile(person, link, color)
    {
        if (link == null)
        {
            //alert("no link specified");
            link = "/";
        }

        return(
        <td onClick={() => {this.redirectTo(link)}} width="50%" style={{background:color}}>
            <center><font style={{fontSize:"4em"}}>{person}</font></center>
        </td>
        );
    }

    redirectTo(link)
    {
        window.location = link;
    }
}

class LoginForm extends React.Component
{
    constructor()
    {
        super();
    }

    renderForm(id,type,placeholder)
    {
        return(<Form id={id} type={type} placeholder={placeholder}/>);
    }

    render()
    {
        return(
        <p>
        {this.renderForm("username","text","username")}<br/>
        {this.renderForm("password","password","password")}<br/>
        <button onClick={() => {this.submitDetails();}}>Submit</button>
        </p>
        );
    }

    submitDetails()
    {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;

        // Do a check here
    }
}

class Form extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <input type={this.props.type} id={this.props.id} placeholder={this.props.placeholder}/>
        );
    }
}

// Rendering below
ReactDOM.render(<Head />, document.getElementById("head"));
ReactDOM.render(<Body />, document.getElementById("body"));
