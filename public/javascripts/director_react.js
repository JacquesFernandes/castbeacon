class Greeting extends React.Component
{
    constructor()
    {
        super();
        //this.name = window.sessionStorage.getItem("name");
    }

    render()
    {
        return(
        <div>
            <font style={{fontSize:"2.5em"}}>Welcome {this.props.name}! <br/>Here's what's there for you today...</font>
        </div>
        );
    }
}
var greeting_checker = setInterval(function()
{
    if (window.sessionStorage.getItem("name")) // name is set
    {
        var greeting = React.createElement(Greeting,{name:window.sessionStorage.getItem("name")});
        console.log("Rendering greeting");
        ReactDOM.render(greeting,document.getElementById("greeting"));
        clearInterval(greeting_checker);
    }
},250);


class SceneRoot extends React.Component // Load empty scenes
{
    constructor()
    {
        super();
        //this.scenes = window.sessionStorage.getItem("scenes");
    }

    render()
    {
        console.log("SCENES: "+this.props.scenes);
        var scene_list = this.props.scenes.split(",");
        return(
        <div>
            <div style={{width:"60%", height:"100%"}}>
                {
                    scene_list.map(function(scene_id)
                    {
                        return(<Scene id={scene_id} />);
                    })
                }
            </div>
        </div>
        );
    }
}

var data_checker = setInterval(function()
{
    if (window.sessionStorage.getItem("scenes")) // loaded scenes
    {
        var scene_root = React.createElement(SceneRoot,{scenes:window.sessionStorage.getItem("scenes")});
        ReactDOM.render(scene_root,document.getElementById("scenes"));
        clearInterval(data_checker);
    }
},250);

class Scene extends React.Component
{
    constructor()
    {
        super();
    }

    goToScene(scene_id)
    {
        window.location = "/director/scene/"+scene_id;
    }

    render()
    {
        return(
            <div>
                <div id={this.props.id} style={{padding:20,border:"solid",borderWidth:"thin",marginBottom:7}} onClick={() => {this.goToScene(this.props.id)}}>
                    <font style={{fontSize:"2em"}}>Scene {this.props.id}</font>
                </div> 
            </div>
        );
    }
}