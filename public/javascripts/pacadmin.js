function addAudience()
{
    // get name
    var name = document.getElementById("audience_name").value;
    if (name === "" || name.trim() === "") // empty
    {
        alert("Forgot Audience member's name...")
        return;
    }
    
    // get number
    var phone = document.getElementById("audience_number").value;
    if (phone === "" || phone.trim() === "") // empty
    {
        alert("Forgot Audience member's phone number...")
        return;
    }

    // push to collection
    $.ajax({
        url:"/PacCity/addAudience",
        method:"POST",
        data:{name:name, phone_number:phone},
        success:function(response)
        {
            console.log("Added Audience: "+JSON.stringify(response));
        }
    });

    // clear fields
    document.getElementById("audience_name").value = "";
    document.getElementById("audience_number").value = "";
}

function addPerformer()
{
    // get name
    var name = document.getElementById("performer_name").value;
    if (name === "" || name.trim() === "") // empty
    {
        alert("Forgot Performer's name...")
        return;
    }
    
    // get number
    var phone = document.getElementById("performer_number").value;
    if (phone === "" || phone.trim() === "") // empty
    {
        alert("Forgot Performer's phone number...")
        return;
    }

    // push to collection
    $.ajax({
        url:"/PacCity/addPerformer",
        method:"POST",
        data:{name:name, phone_number:phone},
        success:function(response)
        {
            console.log("Added Performer and Audience: "+JSON.stringify(response));
        }
    });

    // clear fields
    document.getElementById("performer_name").value = "";
    document.getElementById("performer_number").value = "";
}