function login()
{
    // REMOVE "type_of_person"
    window.sessionStorage.clear();
    var otp = document.getElementById("otp").value;
    if (otp.length !== 6)
    {
        alert("The OTP is 6 digits long...")
        return;
    }
    
    otp = Number(otp);

    if (isNaN(otp))
    {
        alert("Sorry, that is an invlaid otp");
        return;
    }
    
    console.log("Attempting with: "+otp);

    checkOTP(otp);
}

function checkSuccess(data)
{
    if (data.error)
    {
        alert("[ERROR] "+data.error)
        return;
    }
    // query was a success
    
    if (data.length == 0)
    {
        alert("[ERROR] OTP was not found...");
        return;
    }

    //alert(data)
    window.location=data;
}

function checkOTP(otp)
{
    $.ajax({
        method:"GET",
        url:"/api/testOTP/"+otp,
        error:null,
        success:checkSuccess
    });
}