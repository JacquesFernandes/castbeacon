function fetchPerformers()
{
    $.ajax({
        url:"/PacCity/performers",
        method:"GET",
        success:function(response)
        {
            console.log("GOT: "+JSON.stringify(response));
            window.sessionStorage.setItem("performers",JSON.stringify(response));
        }
    });
}

function upload()
{
    var to_artist = null;
    var performers = document.getElementsByName("performer"); // "list" object.. cannot use map apparently
    for (var i = 0; i < performers.length; i++)
    {
        if (performers[i].checked === true)
        {
            console.log("checking: "+performers[i].value)
            to_artist = performers[i].value;
            break;
        }
    }

    var data = {
        to_artist: to_artist,
        message: document.getElementById("comment").value,
        from_name: window.sessionStorage.getItem("name")
    }
    
    $.ajax({
        url:"/PacCity/comment",
        method:"POST",
        data:data,
        success:function(response)
        {
            //alert(response);
            alert(response);
            document.getElementById("comment").value="";
        }
    });
}

fetchPerformers();