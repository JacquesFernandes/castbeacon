var scene_id = window.location.href.split("/").pop();
//console.log(window.sessionStorage.getItem(scene_id))
//console.log(typeof(window.sessionStorage.getItem(scene_id)))
var scene_data = JSON.parse(window.sessionStorage.getItem(scene_id));
console.log(scene_data)

class ActorList extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        var actor_list = scene_data.actors;
        return(
            <div>
            <ul>
                {
                    actor_list.map(function(actor)
                    {
                        return(<li><font style={{fontSize:"2em"}}>{actor}</font></li>);
                    })
                }
            </ul>
            </div>
        );
    }
}
var actor_list = React.createElement(ActorList);
ReactDOM.render(actor_list,document.getElementById("actor_list"));

class Brief extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <div>
            <textarea id="brief_text" style={{width:"100%",border:"3px solid #cccccc", fontFamily:"Serif", fontSize:"1.5em"}}>
                {scene_data.brief}
            </textarea>
            </div>
        );
    }
}

var text_brief = React.createElement(Brief);
ReactDOM.render(text_brief,document.getElementById("brief"));