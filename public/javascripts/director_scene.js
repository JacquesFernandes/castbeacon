function updateBrief()
{
    var scene_id = window.location.href.split("/").pop();
    var url = "/api/updateBrief/"+scene_id;
    var new_data = {"text":document.getElementById("brief_text").value}; // NEEDS TO BE CLEANED
    $.ajax({
        url:url,
        method:"POST",
        data:new_data,
        success:function(response)
        {
            //alert("Successfully updated brief with: "+response);
            console.log(scene_id+" is being updated with "+JSON.stringify(response));
            window.sessionStorage.setItem(scene_id,JSON.stringify(response));
            console.log(window.sessionStorage.getItem(scene_id));
        }
    });
}