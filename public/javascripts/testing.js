class TextComponent extends React.Component
{
    constructor()
    {
        super();
        this.state ={};
        this.something = this.something.bind(this);
    }

    componentWillMount()
    {
        this.props.text = this.props.text? this.props.text : "Nothing to see";
    }

    something()
    {
        alert("ay yo");
    }

    render()
    {
        return(
            <div>
                {this.props.text}
            </div>
        );
    }

}

var text = React.createElement(TextComponent);
ReactDOM.render(text,document.getElementById("text"));

class Container extends React.Component
{
    constructor()
    {
        super();

        this.state = {
            text:""
        };
        this.text = "";
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    handleTextChange(event)
    {
        this.setState({text:event.target.value})
    }

    handleButtonClick(event)
    {
        console.log(this.state.text);
        event.preventDefault();
    }

    render()
    {
        return(
            <div>
                <form>
                    <input type="text" value={this.state.text} onChange={(event) => this.handleTextChange(event)} /><button onClick={(event) => this.handleButtonClick(event)}>Click</button>
                </form>
                <ListBox />
            </div>
        );
    }
}


class ListBox extends React.Component
{
    constructor()
    {
        super();
    }

    render()
    {
        return(
            <div>Under Construction</div>
        );
    }
}

ReactDOM.render(<Container />, document.getElementById("dynamic_listing"));