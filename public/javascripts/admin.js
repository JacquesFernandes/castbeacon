function createActor()
{
    var id = document.getElementById("id").value;
    var otp = document.getElementById("otp").value;
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var sex = document.getElementsByName("sex").value;
    var scene_id = document.getElementById("actor_scene").value;
    var dob = document.getElementById("dob").value; // is in yyyy/mm/dd format

    dob = dob.split("-").reverse().join("/"); // Fixed format to dd/mm/yyyy

    var data = {
        actor_id:id,
        otp:otp,
        name:name,
        email:email,
        phone:phone,
        sex:sex,
        dob:dob,
        scene_id:scene_id
    };
    
    var url = "/api/addActorData";
    $.ajax({
        url:url,
        data:data,
        method:"POST",
        success:(response) => {alert(response)}
    });
}

function createScene()
{
    var scene_data = {};
    var director_data = {};

    scene_data.scene_id = document.getElementById("scene_id").value;
    scene_data.director = document.getElementById("director").value;
    scene_data.brief = document.getElementById("brief").value;
    scene_data.actors = document.getElementById("actors").value;

    director_data.otp = document.getElementById("dir_otp").value;
    director_data.name = scene_data.director;
    director_data.type_of_person = "director";
    
    url = "/api/addDirector";
    $.ajax({
        url:url,
        method:"POST",
        data:director_data,
        success:function(response){
            alert(response);
            // Adding the scene now...
            // cleaning
            scene_data.actors = scene_data.actors.split(", ");
            console.log(scene_data.actors);
            var url = "/api/addScene";
            $.ajax({
                url:url,
                method:"POST",
                data:scene_data,
                success:(response) => {alert(response)}
            });
        }
    });


}