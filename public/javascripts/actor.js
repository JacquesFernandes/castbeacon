function fetchData()
{
    var director = null; // director object
    var otp = window.location.href.split("/").pop();
    console.log(otp);
    // fetch actor's name and brief 
    $.ajax({
        url:"/api/actor/"+otp,
        method:"GET",
        success:function(response)
        {
            director = response;
            name = director.name;
            window.sessionStorage.setItem("name",name); // Director Name
            console.log("Loaded Actor Name");
            
            // get scene brief
            var scene_id = response.scene_id;
            $.ajax({
                url:"/api/scene/"+scene_id,
                method:"GET",
                success:function(scene)
                {
                    window.sessionStorage.setItem("brief",scene.brief);
                    console.log("got brief: "+scene.brief);
                }
            });
        }
    })
}

function saveNote()
{
    var notes = document.getElementById("notes").value;
    document.sessionStorage.setItem("notes",notes);
}

function loadNote()
{
    if (document.sessionStorage.getItem("notes")) // notes are present
    {
        document.getElementById("notes").value = document.sessionStorage.getItem("notes");
    }
}

// Auto-run code below
if (!window.sessionStorage.getItem("type_of_person")) // if this is not set
{
    window.sessionStorage.setItem("type_of_person","actor");
}

if (window.sessionStorage.getItem("type_of_person") !== "actor")
{
    alert("Nice try, please sign in and come back :)")
    window.location = "/";
}

var otp = window.location.href.split("/").pop();
var name = null;
fetchData();
